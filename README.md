Requirements:
    NodeJS v5.0 +
    
First run `npm install` in root folder to install all dependencies.

After successful installation you also have builded production version of project.

Project was builded with GulpJS, so you have 3 gulp tasks:

`gulp` - build styles and js
`gulp prod` - build styles, js and minify it
`gulp w` - build styles,js and run watcher

