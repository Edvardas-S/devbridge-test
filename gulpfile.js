'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence');
var purify = require('gulp-purifycss');

// build CSS
gulp.task('style', function(){
    gulp.src('scss/main.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('css/'))
});

// build JavaScript
gulp.task('js', function(){
    return gulp.src(['node_modules/jquery/dist/jquery.js',
                    'js/extra/*.js',
                    'js/common/*.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('js/'));
});

// watch for changes and automatically build CSS/JS
gulp.task('watch', function () {
    watch('scss/**/*.scss', function(){
        gulp.start('style');
    });
    watch(['js/extra/*.js',
        'js/common/*.js'], function(){
        gulp.start('js');
    });
});

// production tasks

gulp.task('prod_style', function () {
    gulp.src('css/main.css')
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(purify(['js/main.js', '*.html']))
        .pipe(gulp.dest('css/'));
});

gulp.task('prod_js', function () {
    return gulp.src('js/main.js')
        .pipe(uglify())
        .pipe(gulp.dest('js/'))
});

// build only static files
gulp.task('default', [
    'style',
    'js'
]);

// automatic build if changed are made
gulp.task('w', [
    'style',
    'js',
    'watch'
]);

gulp.task('prod', function() {
    runSequence(
        'js',
        'style',
        'prod_js',
        'prod_style'
    );
});
