var page = (function (window) {
    function heights() {
        if ($(window).width() >= 768) {
            function jsEqh1() {
                $(".jsEqh-1").height("auto").equalHeights();
            }jsEqh1();
        }
    }

    function validateForm() {
        $(document).ready(function ($) {
            if($('form[name="main"]').length == 0) return false;

            var $form = $('form[name="main"]'),
                $successMsg = $('.alert'),
                $formButton = $form.find('button[type="submit"]');
            $form.parsley().on('form[name="main"]:submit', function () {
                $successMsg.show();
                return false;
            })

            $form.submit(function (e) {
                e.preventDefault();
                if ($(this).parsley().isValid()) {
                    var data = {
                        name: $("#form-name").val(),
                        lastname: $("#form-lastname").val(),
                        message: $("#form-text").val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "email.php",
                        data: data,
                        success: function () {
                            $formButton.html('Thank you!').prop('disabled', true);
                        },
                        error: function () {
                            $formButton.html('Please try again');
                        }
                    });
                    return false;
                }
            })
        })
    }

    return {
        heights: heights,
        validateForm: validateForm
    }
})(window);

page.heights();
page.validateForm();
