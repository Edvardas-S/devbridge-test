var Header = (function (window) {

    function header() {
        if ($('nav.main').length == 0) return false;

        var nav = $('nav.main');
        var touchMenu = nav.find('.touch-menu');
        var menuWrap = $('ul.top');
        var ww = $(window).width();
        var opened = false;

        function toggleMenu() {
            if (opened) {
                menuWrap.slideUp(function () {
                    menuWrap.hide();
                });
                touchMenu.find('span').removeClass('active');
                opened = false;
            } else {
                menuWrap.slideDown();
                touchMenu.find('span').addClass('active');
                opened = true;
            }

        }

        touchMenu.on('click', function (e) {
            if (ww <= 768) {
                e.preventDefault();
                toggleMenu();
            }
        });

        menuWrap.on('click', function (e) {
            if (ww <= 768) {
                toggleMenu();
            }
        });

    $(window).resize(function () {
        ww = $(window).width();
        if (ww > 768 && menuWrap.is(':hidden')) {
            menuWrap.removeAttr('style');
        }
    });
}

return {
    header: header
}
})
(window);

Header.header();
